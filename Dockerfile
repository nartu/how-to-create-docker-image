FROM python:3

WORKDIR /usr/src/app

COPY ./requirements.txt ./
RUN cat requirements.txt
RUN pip install -U pip && pip install --no-cache-dir -r requirements.txt

VOLUME [ "/var/log/check-domain" ]

COPY ./app .

EXPOSE 80

CMD [ "python", "-u", "main.py" ]
